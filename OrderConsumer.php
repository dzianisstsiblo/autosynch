<?php
namespace Autosynch;


//use Magento\Sales\Api\DataOrderInterface;
//use Autosynch\Sale\DataTransferInterface;

class OrderConsumer
{
    /**
    * @var \Zend\Log\Logger
    */
    private $logger;

    /**
    * @var string
    */
    private $logFileName = 'order-place-consumer.log';

    /**
    * @var \Magento\Framework\App\Filesystem\DirectoryList
    */
    private $directoryList;

    /**
    * DeleteConsumer constructor.
    * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    * @throws \Magento\Framework\Exception\FileSystemException
    */
    public function __construct(\Magento\Framework\App\Filesystem\DirectoryList $directoryList) {
        $this->directoryList = $directoryList;
        $logDir = $directoryList->getPath('log');
        $writer = new \Zend\Log\Writer\Stream($logDir . DIRECTORY_SEPARATOR . $this->logFileName);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $this->logger = $logger;
    }

    /**
     * @return void
     */
    public function processMessage()
    {
        //$this->logger->info($order->getId() . ' ' . $order->getStatus());
        $this->sendOrderInfoToGBS();
        //exit('hello');
    }

    public function sendOrderInfoToGBS()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://bb5hgbxnt2.execute-api.eu-west-1.amazonaws.com/api/v1/packages",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => "{\n  \"EventEditionCode\" : \"IJL--19\",\n  \"OrganisationRxMasterId\" : \"OCDH_OBU_000-ORG-3385\",\n  \"Package\" : \"Gold\",\n  \"CorrelationId\" : \"123456789\"\n\n}",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $this->logger->info('response from gateway ' . $response);

        if (!empty($err)) {
            $this->logger->info('error from gateway ' . $err);
        }
    }


    public function getPayload()
    {
        return [
            "EventEditionCode" => "IJL--19",
            "OrganisationRxMasterId" => "OCDH_OBU_000-ORG-3385",
            "Package" => "Gold",
            "CorrelationId" => "123456789"
        ];
}

}
