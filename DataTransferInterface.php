<?php
namespace Autosynch;

interface DataTransferInterface{

    /**
     * @return mixed
     */
    public function getEventEditionCode();

    /**
     * @param mixed $eventEditionCode
     * @return void
     */
    public function setEventEditionCode($eventEditionCode);

    /**
     * @return mixed
     */
    public function getOrganisationRxMasterId();

    /**
     * @param mixed $organisationRxMasterId
     * @return void
     */
    public function setOrganisationRxMasterId($organisationRxMasterId);

    /**
     * @return mixed
     */
    public function getPackage();

    /**
     * @param mixed $package
     * @return void
     */
    public function setPackage($package);

    /**
     * @return mixed
     */
    public function getCorrelationId();

    /**
     * @param mixed $correlationId
     * @return void
     */
    public function setCorrelationId($correlationId);
}
