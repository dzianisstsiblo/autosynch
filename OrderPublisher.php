<?php
namespace Autosynch;

use Magento\Framework\MessageQueue\PublisherInterface;
use Autosynch\Sale\DataTransferInterface;

class OrderPublisher
{
    const TOPIC_NAME = 'reedexpo.order.place';

    /**
    * @var PublisherInterface
    */
    private $publisher;

    /**
    * @param PublisherInterface $publisher
    */
    public function __construct(PublisherInterface $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
    * {@inheritdoc}
    */
    public function execute(DataTransferInterface $data)
    {
        $this->publisher->publish(self::TOPIC_NAME, $data);
    }
}
