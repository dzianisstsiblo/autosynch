<?php
namespace Autosynch\Sale;

use Autosynch\DataTransferInterface;

class DataTransfer implements DataTransferInterface
{
    public $eventEditionCode;

    public $organisationRxMasterId;

    public $package;

    public $correlationId;

    /**
     * @return mixed
     */
    public function getEventEditionCode()
    {
        return $this->eventEditionCode;
    }

    /**
     * @param mixed $eventEditionCode
     */
    public function setEventEditionCode($eventEditionCode)
    {
        $this->eventEditionCode = $eventEditionCode;
    }

    /**
     * @return mixed
     */
    public function getOrganisationRxMasterId()
    {
        return $this->organisationRxMasterId;
    }

    /**
     * @param mixed $organisationRxMasterId
     */
    public function setOrganisationRxMasterId($organisationRxMasterId)
    {
        $this->organisationRxMasterId = $organisationRxMasterId;
    }

    /**
     * @return mixed
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param mixed $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @return mixed
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param mixed $correlationId
     */
    public function setCorrelationId($correlationId)
    {
        $this->correlationId = $correlationId;
    }
}
